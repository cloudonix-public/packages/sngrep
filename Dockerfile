FROM centos:7
RUN yum install -q -y ncurses-devel make libpcap-devel pcre-devel openssl-devel git gcc autoconf automake rpm-build zip
WORKDIR /src
RUN git clone https://github.com/irontec/sngrep sngrep-1.4.6
RUN mkdir -p /root/rpmbuild/SOURCES
RUN zip -r /root/rpmbuild/SOURCES/v1.4.6.zip sngrep-1.4.6
RUN rpmbuild -ba sngrep-1.4.6/pkg/rpm/SPECS/sngrep.spec --with openssl
#RUN ./bootstrap.sh
#RUN ./configure
#RUN make
#RUN make install DESTDIR=/build
